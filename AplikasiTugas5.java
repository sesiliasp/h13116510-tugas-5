import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class AplikasiTugas5 {
		static Scanner input=new Scanner(System.in);
		
		static Object[][] arr=new Object[11][5];
		
		static int hitung=0;
		static String pilihan;
		static String menupilihan;
		static int kode;
		static String nama;
		static String warna;
		static int jumlah;
		static int harga;
		
		  public static void main(String[]args) throws IOException{
			  	PrintWriter out = new PrintWriter("Data.txt");
				PrintWriter sort = new PrintWriter("DataTerurut.txt");
			try{
			
			boolean berakhir;
			do{
				System.out.println("1. Tambah Barang");
				System.out.println("2. Menampilkan Barang");
				
				System.out.print("---> ");
				menupilihan = input.nextLine();
				
				if(menupilihan.contentEquals("0")){
					break;
				}
				
				switch(menupilihan){
				case "1":
					
					 do{
						 
						 berakhir=true;
						 
						 do{
							 
								 System.out.print("Kode : ");
							 try{
								 kode=input.nextInt();
								 break;
								 
							 }catch(InputMismatchException e){
								
								 System.out.println("Kode harus dalam bentuk Angka");
								 System.out.println("Coba Lagi!");
								 input.nextLine();
								 continue;
								 
							 }
						 }while(true);
						  
						 for(int index=0;index<hitung;index++){
							 if(kode==(int)arr[index][0]){
								 berakhir=false;
								 System.out.println("Kode Telah digunakan");
								 System.out.println("Coba Lagi!\n");
							 }
							 
						 }
						 
					 }while(berakhir==false);
					 input.nextLine();
					 do{
						 berakhir=true;
						 System.out.print("Nama : ");
						 nama=input.nextLine();
						 for(int index=0;index<hitung;index++){
							 if(nama.contentEquals((String) arr[index][1])){
								 berakhir=false;
								 System.out.println("Nama Telah digunakan");
								 System.out.println("Coba Lagi!\n");
							 }
							 
						 }
						 if(nama.matches(".*\\d+.*")){
							 berakhir=false;
							 System.out.println("Nama harus dalam bentuk Huruf");
							 System.out.println("Coba Lagi!\n");
						 }
						 
					 }while(berakhir==false);
					 
					 do{
						 berakhir=true;
						 System.out.print("Warna : ");
						 warna=input.nextLine();
						 if(warna.matches(".*\\d+.*")){
							 berakhir=false;
							 System.out.println("Warna harus dalam bentuk Huruf");
							 System.out.println("Coba Lagi!\n");
						 }
						 
						 
					 }while(berakhir==false);
					
					 
						 
						 do{
							 
								 System.out.print("Total stock : ");
							 try{
								 jumlah=input.nextInt();
								 break;
								 
							 }catch(InputMismatchException e){
								
								 System.out.println("Total Stock harus dalam bentuk Angka");
								 System.out.println("Coba Lagi!");
								 input.nextLine();
								 continue;
								 
							 }
						 }while(true);
						  
						 
					 do{
							 
							 System.out.print("Harga : ");
						 try{
							 harga=input.nextInt();
							 break;
							 
						 }catch(InputMismatchException e){
							
							 System.out.println("Harga harus dalam bentuk Angka");
							 System.out.println("Coba Lagi!");
							 input.nextLine();
							 continue;
							 
						 }
					 }while(true);
					 
					
					    arr[hitung][0] = kode;
					    arr[hitung][1] = nama;
					    arr[hitung][2] = warna;
					    arr[hitung][3] = jumlah;
					    arr[hitung][4] = harga;
					    
					    out.println(kode);
						out.println(nama);
						out.println(warna);
						out.println(jumlah);
						out.println(harga);
						out.println("====");
						
						
					    
					   hitung++;
					input.nextLine();
					break;
					
					
					
				case "2":
					System.out.println("Sort by: ");
					System.out.println("1. Kode");
					System.out.println("2. Nama");
					System.out.println("3. Warna");
					System.out.println("4. Jumlah");
					System.out.println("5. Harga");
					System.out.print("----> ");
					pilihan=input.nextLine();
					
					
					switch(pilihan){
					case "1":
						
						String temp="";
						int tempInt=-1;
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<hitung;IndexKedua++){
								if( ( (int) arr[index][0] ) < ((int)arr[IndexKedua][0])){
									
									 tempInt = (int) arr[IndexKedua][0];  
	                                 arr[IndexKedua][0] = arr[index][0];  
	                                 arr[index][0] = tempInt;
	                                 
	                                 temp = (String) arr[IndexKedua][1];  
	                                 arr[IndexKedua][1] = arr[index][1];  
	                                 arr[index][1] = temp;
	                                 
	                                 temp = (String) arr[IndexKedua][2];  
	                                 arr[IndexKedua][2] = arr[index][2];  
	                                 arr[index][2] = temp;
	                                 
	                                 tempInt = (int) arr[IndexKedua][3];  
	                                 arr[IndexKedua][3] = arr[index][3];  
	                                 arr[index][3] = tempInt;
	                                 
	                                 tempInt = (int) arr[IndexKedua][4];  
	                                 arr[IndexKedua][4] = arr[index][4];  
	                                 arr[index][4] = tempInt;
								}
							}
						}
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<5;IndexKedua++){
								sort.println(arr[index][IndexKedua]);
							}
							sort.println("====");
						}
						sort.close();
						System.out.println("DataTerurut.txt berhasil dimuat");
						break;
						
					case "2":
						temp="";
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<hitung;IndexKedua++){
								if(((String) arr[index][1]).compareToIgnoreCase((String) arr[IndexKedua][1]) < 0){
									
									tempInt = (int) arr[IndexKedua][0];  
	                                arr[IndexKedua][0] = arr[index][0];  
	                                arr[index][0] = tempInt;
	                                
	                                temp = (String) arr[IndexKedua][1];  
	                                arr[IndexKedua][1] = arr[index][1];  
	                                arr[index][1] = temp;
	                                
	                                temp = (String) arr[IndexKedua][2];  
	                                arr[IndexKedua][2] = arr[index][2];  
	                                arr[index][2] = temp;
	                                
	                                tempInt = (int) arr[IndexKedua][3];  
	                                arr[IndexKedua][3] = arr[index][3];  
	                                arr[index][3] = tempInt;
	                                
	                                tempInt = (int) arr[IndexKedua][4];  
	                                arr[IndexKedua][4] = arr[index][4];  
	                                arr[index][4] = tempInt;
								}
							}
						}
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<5;IndexKedua++){
								sort.println(arr[index][IndexKedua]);
							}
							sort.println("====");
						}
						sort.close();
						System.out.println("DataTerurut.txt berhasil dimuat");
						break;
						
					case "3":
						temp="";
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<hitung;IndexKedua++){
								if(((String) arr[index][2]).compareToIgnoreCase((String) arr[IndexKedua][2]) < 0){
									
									tempInt = (int) arr[IndexKedua][0];  
	                                arr[IndexKedua][0] = arr[index][0];  
	                                arr[index][0] = tempInt;
	                                
	                                temp = (String) arr[IndexKedua][1];  
	                                arr[IndexKedua][1] = arr[index][1];  
	                                arr[index][1] = temp;
	                                
	                                temp = (String) arr[IndexKedua][2];  
	                                arr[IndexKedua][2] = arr[index][2];  
	                                arr[index][2] = temp;
	                                
	                                tempInt = (int) arr[IndexKedua][3];  
	                                arr[IndexKedua][3] = arr[index][3];  
	                                arr[index][3] = tempInt;
	                                
	                                tempInt = (int) arr[IndexKedua][4];  
	                                arr[IndexKedua][4] = arr[index][4];  
	                                arr[index][4] = tempInt;
								}
							}
						}
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<5;IndexKedua++){
								sort.println(arr[index][IndexKedua]);
							}
							sort.println("====");
						}
						sort.close();
						System.out.println("DataTerurut.txt berhasil dimuat");
						break;
						
					case "4":
						temp="";
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<hitung;IndexKedua++){
								if( ( (int) arr[index][3] ) < ((int)arr[IndexKedua][3])){
									
									tempInt = (int) arr[IndexKedua][0];  
	                                arr[IndexKedua][0] = arr[index][0];  
	                                arr[index][0] = tempInt;
	                                
	                                temp = (String) arr[IndexKedua][1];  
	                                arr[IndexKedua][1] = arr[index][1];  
	                                arr[index][1] = temp;
	                                
	                                temp = (String) arr[IndexKedua][2];  
	                                arr[IndexKedua][2] = arr[index][2];  
	                                arr[index][2] = temp;
	                                
	                                tempInt = (int) arr[IndexKedua][3];  
	                                arr[IndexKedua][3] = arr[index][3];  
	                                arr[index][3] = tempInt;
	                                
	                                tempInt = (int) arr[IndexKedua][4];  
	                                arr[IndexKedua][4] = arr[index][4];  
	                                arr[index][4] = tempInt;
								}
							}
							
						}
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<5;IndexKedua++){
								sort.println(arr[index][IndexKedua]);
							}sort.println("====");
						}
						sort.close();
						System.out.println("DataTerurut.txt berhasil dimuat");
						break;
						
					case "5":
						temp="";
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<hitung;IndexKedua++){
								if( ( (int) arr[index][4] ) < ((int)arr[IndexKedua][4])){
									
									tempInt = (int) arr[IndexKedua][0];  
	                                arr[IndexKedua][0] = arr[index][0];  
	                                arr[index][0] = tempInt;
	                                
	                                temp = (String) arr[IndexKedua][1];  
	                                arr[IndexKedua][1] = arr[index][1];  
	                                arr[index][1] = temp;
	                                
	                                temp = (String) arr[IndexKedua][2];  
	                                arr[IndexKedua][2] = arr[index][2];  
	                                arr[index][2] = temp;
	                                
	                                tempInt = (int) arr[IndexKedua][3];  
	                                arr[IndexKedua][3] = arr[index][3];  
	                                arr[index][3] = tempInt;
	                                
	                                tempInt = (int) arr[IndexKedua][4];  
	                                arr[IndexKedua][4] = arr[index][4];  
	                                arr[index][4] = tempInt;
								}
							}
							
						}
						for(int index=0;index<hitung;index++){
							for(int IndexKedua=0;IndexKedua<5;IndexKedua++){
								sort.println(arr[index][IndexKedua]);
							}sort.println("====");
						}
						sort.close();
						System.out.println("DataTerurut.txt berhasil dimuat");
						break;
					default:
						
						break;
							
					}
					
					out.close();
					System.out.println("Data.txt berhasil dimuat");
					break;
				
				default:
					System.out.println("Piih angka 1 atau 2 ");
					System.out.println("Coba Lagi\n");
					break;
				}
						
				
			}while(menupilihan.contentEquals("2")==false);		
		}finally{
			out.close();
			sort.close();
		}
	  }
}